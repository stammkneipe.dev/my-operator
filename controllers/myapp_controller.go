/*
Copyright 2024.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"

	corev1 "k8s.io/api/core/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"

	examplev1alpha1 "github.com/my-group/my-operator/api/v1alpha1"
)

// MyAppReconciler reconciles a MyApp object
type MyAppReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

//+kubebuilder:rbac:groups=example.stammkneipe.dev,resources=myapps,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=example.stammkneipe.dev,resources=myapps/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=example.stammkneipe.dev,resources=myapps/finalizers,verbs=update

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the MyApp object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.14.1/pkg/reconcile
func (r *MyAppReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	_ = log.FromContext(ctx)

	// Get CRD
	myApp := &examplev1alpha1.MyApp{}
	if err := r.Get(ctx, req.NamespacedName, myApp); err != nil {
		if apierrors.IsNotFound(err) {
			log.Log.Info("MyApp not found. Ignoring since object must be deleted.")
			return ctrl.Result{}, nil
		}
		log.Log.Error(err, "Failed to get MyApp.")
	}

	// Start the Reconciliation
	conditions := &myApp.Status.Conditions
	if len(*conditions) == 0 {
		meta.SetStatusCondition(conditions, metav1.Condition{
			Type:    "App",
			Status:  metav1.ConditionUnknown,
			Reason:  "Initializing",
			Message: "Starting reconciliation",
		})
		log.Log.Info("Condition", "Length", len(myApp.Status.Conditions))
		if err := r.Status().Update(ctx, myApp); err != nil {
			log.Log.Error(err, "Failed to update MyApp status")
			return ctrl.Result{}, err
		}
		// Start the next step
		return ctrl.Result{}, nil
	}

	// Act depending on the Condition. This is just a rough example.
	currentCondition := (*conditions)[0].Reason
	switch currentCondition {
	case "Initializing":
		// Create a ConfigMap
		cm := &corev1.ConfigMap{}
		err := r.Get(ctx, types.NamespacedName{Name: myApp.Name, Namespace: myApp.Namespace}, cm)
		if err != nil {
			if apierrors.IsNotFound(err) {
				// No ConfigMap exists and we create one
				cmInstance := &corev1.ConfigMap{
					ObjectMeta: metav1.ObjectMeta{
						Name:      myApp.Name,
						Namespace: myApp.Namespace,
					},
				}
				if err := r.Create(ctx, cmInstance); err != nil {
					log.Log.Error(err, "Failed to create a new ConfigMap", "Namespace", myApp.Namespace, "Name", myApp.Name)
					return ctrl.Result{}, err
				}
			} else {
				// Some unknown error occurred
				log.Log.Error(err, "Failed to get ConfigMap", "Namespace", myApp.Namespace, "Name", myApp.Name)
				return ctrl.Result{}, err
			}
		}
		// Update the status
		log.Log.Info("Config Map created")
		meta.SetStatusCondition(&myApp.Status.Conditions, metav1.Condition{
			Type:    "App",
			Status:  metav1.ConditionTrue,
			Reason:  "Available",
			Message: "Config Map created",
		})
		if err := r.Status().Update(ctx, myApp); err != nil {
			log.Log.Error(err, "Failed to update status")
			return ctrl.Result{}, err
		}
	case "Unavailable":
		// Retry depending on the error
	case "Available":
		// Everything is fine
	default:
		// Set State if State was unknown
		meta.SetStatusCondition(conditions, metav1.Condition{
			Type:    "App",
			Status:  metav1.ConditionUnknown,
			Reason:  "Initializing",
			Message: "Starting reconciliation",
		})
		if err := r.Status().Update(ctx, myApp); err != nil {
			log.Log.Error(err, "Failed to update myApp status")
			return ctrl.Result{}, err
		}
	}

	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *MyAppReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&examplev1alpha1.MyApp{}).
		Complete(r)
}
